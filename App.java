package org.inguelberth.ui;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.RadioButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.Scene;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.input.MouseEvent;
import java.util.EventObject;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.scene.control.ContentDisplay;

public class App extends Application implements EventHandler<Event>{
	
	private HBox hboxPane, hboxRB;
	private GridPane gridPane;
	private VBox vboxPane;
	private BorderPane borderPane;
	private Label lblLabel;
	private Button btnUno, btnDos, btnTres, btn1, btn2, btn3, btn4;
	private TextField tfUno;
	private PasswordField pfUno;
	private CheckBox chkUno;
	private ToggleGroup tgUno;
	private RadioButton rb1, rb2, rb3;
	private ImageView imgView, imgIcon;
	
	private Scene primaryScene;


	public void start(Stage primaryStage){

		btnUno = new Button("Boton uno");
		btnUno.addEventHandler(ActionEvent.ACTION, this);

		btnDos = new Button("Boton dos");
		btnDos.addEventHandler(ActionEvent.ACTION, this);

		btnTres = new Button("Boton tres");
		btnTres.addEventHandler(ActionEvent.ACTION, this);

		lblLabel = new Label("Hola!");
		lblLabel.addEventHandler(MouseEvent.MOUSE_ENTERED, this);
		lblLabel.addEventHandler(MouseEvent.MOUSE_EXITED, this);
		
		hboxPane = new HBox();
		hboxPane.setAlignment(Pos.CENTER);
		hboxPane.setStyle("-fx-background-color: #E3AA43;");
		
		hboxPane.getChildren().add(btnUno);
		hboxPane.getChildren().add(btnDos);
		hboxPane.getChildren().add(btnTres);
		hboxPane.getChildren().add(lblLabel);

		imgIcon = new ImageView(new Image(this.getClass().getResourceAsStream("tux.jpg"), 32, 32, true, true));		

		btn1 = new Button("B1", imgIcon);
		btn1.setPrefWidth(200);
		btn1.setPrefHeight(100);
		btn1.setContentDisplay(ContentDisplay.LEFT);

		btn2 = new Button("B2");
		btn3 = new Button("B3");
		btn4 = new Button("B4");

		vboxPane = new VBox();
		vboxPane.setAlignment(Pos.TOP_CENTER);
		vboxPane.setPadding(new Insets(10,10,10,10));
		vboxPane.setStyle("-fx-background-color: #825794;");

		vboxPane.getChildren().add(btn2);
		vboxPane.getChildren().add(btn3);
		vboxPane.getChildren().add(btn4);
		
		vboxPane.getChildren().add(btn1);
		VBox.setMargin(btn1, new Insets(50,0 ,50 , 0));

		tfUno = new TextField();
		tfUno.setPromptText("TEXTO");

		pfUno = new PasswordField();
		pfUno.setPromptText("PASSWORD");
		pfUno.setMaxWidth(200);

		chkUno = new CheckBox("Check");

		tgUno = new ToggleGroup();
		
		rb1 = new RadioButton("OP1");
		rb1.setToggleGroup(tgUno);	
		rb2 = new RadioButton("OP2");
		rb2.setToggleGroup(tgUno);		
		rb3 = new RadioButton("OP3");		
		rb3.setToggleGroup(tgUno);

		hboxRB = new HBox();
		hboxRB.getChildren().add(rb1);
		hboxRB.getChildren().add(rb2);
		hboxRB.getChildren().add(rb3);

		imgView = new ImageView(new Image(this.getClass().getResourceAsStream("imagen.jpg")));

		gridPane = new GridPane();
		gridPane.setStyle("-fx-background-color: #BAD2EA;");
		gridPane.setAlignment(Pos.CENTER);		
		gridPane.setVgap(100);
		gridPane.setHgap(200);
		gridPane.setPadding(new Insets(50,50,50,50));

		gridPane.add(tfUno, 0, 0);
		gridPane.add(pfUno, 1, 0);
		gridPane.add(chkUno, 0, 1);
		gridPane.add(hboxRB, 1, 1);
		gridPane.add(imgView, 0,2, 2, 2);

		borderPane = new BorderPane();
		borderPane.setTop(hboxPane);
		borderPane.setRight(vboxPane);
		borderPane.setCenter(gridPane);
		
		primaryScene =new Scene(borderPane);

		

		primaryStage.setScene(primaryScene);
		primaryStage.getIcons().add(new Image(this.getClass().getResourceAsStream("imagen.jpg")));				
		primaryStage.setTitle("Primer ejemplo");
		primaryStage.setMaximized(true);
		primaryStage.show();
	}
        @Override
	public void handle(Event event){
		if(event instanceof MouseEvent){
			if(event.getEventType()==MouseEvent.MOUSE_ENTERED){
				lblLabel.setText("Entro");
			}else if(event.getEventType()==MouseEvent.MOUSE_EXITED){
				lblLabel.setText("Salio");
			}
		}else if(event instanceof ActionEvent){
			if(event.getSource().equals(btnUno))
				System.out.println("Presionaste UNO!");
			else if(((Button)event.getSource()).getText().equals(btnDos.getText()))
				System.out.println("Presionaste DOS!");
		}	
	}
}
